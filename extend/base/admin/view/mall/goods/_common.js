var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'mall.goods/index',
    add_url: 'mall.goods/add',
    edit_url: 'mall.goods/edit',
    delete_url: 'mall.goods/delete',
    export_url: 'mall.goods/export',
    modify_url: 'mall.goods/modify',
    stock_url: 'mall.goods/stock',
    read_url: 'mall.goods/read',
    formFullScreen: true,
};