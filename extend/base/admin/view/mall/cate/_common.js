var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'mall.cate/index',
    add_url: 'mall.cate/add',
    edit_url: 'mall.cate/edit',
    delete_url: 'mall.cate/delete',
    export_url: 'mall.cate/export',
    modify_url: 'mall.cate/modify',
};