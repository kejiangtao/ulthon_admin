var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'mall.tag/index',
    add_url: 'mall.tag/add',
    edit_url: 'mall.tag/edit',
    delete_url: 'mall.tag/delete',
    export_url: 'mall.tag/export',
    modify_url: 'mall.tag/modify',
};
