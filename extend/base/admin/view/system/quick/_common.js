var iconPickerFa = layui.iconPickerFa,
    autocomplete = layui.autocomplete;

var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'system.quick/index',
    add_url: 'system.quick/add',
    edit_url: 'system.quick/edit',
    delete_url: 'system.quick/delete',
    export_url: 'system.quick/export',
    modify_url: 'system.quick/modify',
};