var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'system.uploadfile/index',
    add_url: 'system.uploadfile/add',
    edit_url: 'system.uploadfile/edit',
    delete_url: 'system.uploadfile/delete',
    modify_url: 'system.uploadfile/modify',
    export_url: 'system.uploadfile/export',
};
