var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'system.auth/index',
    add_url: 'system.auth/add',
    edit_url: 'system.auth/edit',
    delete_url: 'system.auth/delete',
    export_url: 'system.auth/export',
    modify_url: 'system.auth/modify',
    authorize_url: 'system.auth/authorize',
};