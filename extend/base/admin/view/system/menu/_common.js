var table = layui.table,
treetable = layui.treetable,
iconPickerFa = layui.iconPickerFa,
autocomplete = layui.autocomplete;

var init = {
table_elem: '#currentTable',
table_render_id: 'currentTableRenderId',
index_url: 'system.menu/index',
add_url: 'system.menu/add',
delete_url: 'system.menu/delete',
edit_url: 'system.menu/edit',
modify_url: 'system.menu/modify',
};
