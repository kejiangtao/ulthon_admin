var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'system.admin/index',
    add_url: 'system.admin/add',
    edit_url: 'system.admin/edit',
    delete_url: 'system.admin/delete',
    modify_url: 'system.admin/modify',
    export_url: 'system.admin/export',
    password_url: 'system.admin/password',
};
