var init = {
    table_elem: '#currentTable',
    table_render_id: 'currentTableRenderId',
    index_url: 'system.node/index',
    add_url: 'system.node/add',
    edit_url: 'system.node/edit',
    delete_url: 'system.node/delete',
    modify_url: 'system.node/modify',
};